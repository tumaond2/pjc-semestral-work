//
// Created by tumao on 11.01.22.
//
#include <Console.h>

std::string Console::color(std::string word, int color, int part) {
    return  "\033[" + std::to_string(part) + ";" + std::to_string(color) + "m" + word + "\033[0m";
}

std::string Console::color(unsigned char word, int color, int part) {
    return  "\033[" + std::to_string(part) + ";" + std::to_string(color) + "m" + std::string(1, word)+ "\033[0m";
}

std::string Console::black (std::string word){
    return color(word, 30);
}
std::string Console::red (std::string word){
    return color(word, 31);
}
std::string Console::red (unsigned char word){
    return color(word, 31);
}

std::string Console::green (std::string word){
    return color(word, 32);
}
std::string Console::yellow (std::string word){
    return color(word, 33, 1);
}
std::string Console::gray (std::string word){
    return color(word, 37, 1);
}

std::string Console::lightGray (std::string word){
    return color(word, 37, 0);
}