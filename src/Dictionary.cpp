//
// Created by tumao on 10.01.22.
//


#include "Dictionary.h"

#include <utility>
#include <atomic>
#include <barrier>

static unsigned long globalMin;
static std::mutex globalMutex;
static bool caseInsensitive = true;

Dictionary::Dictionary(std::string filename, unsigned int numThreads) {
    for (int i = 0; i < numThreads; i++){
        auto worker = new DictionaryWorker();
        worker->Start();
        workers.push_back(worker);
    }

    const int blockSize = (int) std::max(int(BUFFER_SIZE / numThreads), MIN_BLOCK_SIZE);

    std::ifstream file(filename, std::ios::binary);

    if (!file.is_open()) {
        std::cout << "failed to open " << filename << '\n';
        exit(1);
    }

    unsigned char* buffer;

    int i = 0;

    buffer = (unsigned char *) malloc(blockSize);

    if (buffer == nullptr) {
        std::cout << "could not allocate " << blockSize << "bytes";
        exit(1);
    }

    int bufferStartPos = 0;

    file.read(reinterpret_cast<char *>(&buffer[bufferStartPos]), blockSize - bufferStartPos);
    std::streamsize totalRead = file.gcount();
    while (file.gcount() == blockSize - bufferStartPos) {
        const int currentReadSize = blockSize - bufferStartPos;
        int prevLineEndIndex = blockSize - 1;
        while (buffer[prevLineEndIndex] != '\n' && buffer[prevLineEndIndex] != 0) {
            prevLineEndIndex--;
        }
        if (buffer[prevLineEndIndex] == '\n') {
            buffer[prevLineEndIndex++] = 0;
        }

        bufferStartPos = 0;
        auto* newBuffer = (unsigned char *) malloc(blockSize);

        if (newBuffer == nullptr) {
            std::cout << "could not allocate " << blockSize << "bytes";
            exit(1);
        }

        while (prevLineEndIndex != blockSize) {
            newBuffer[bufferStartPos++] = buffer[prevLineEndIndex++];
        }

        workers[i % numThreads]->loadWords(buffer);


        buffer = newBuffer;
        file.read(reinterpret_cast<char *>(&buffer[bufferStartPos]), blockSize - bufferStartPos);
        totalRead += file.gcount();
        i++;
    }

//    std::cout << "read: " << totalRead << '\n';

    buffer[file.gcount()] = 0;
    workers[i % numThreads]->loadWords(buffer);

    this->awaitAll();
}

unsigned long Dictionary::getWordCount() {
    unsigned long words = 0;
    for (auto worker : workers){
        words += worker->getWordCount();
    }
    return words;
}




std::vector<result> Dictionary::getClosest(const std::string& word) {
    globalMin = INT32_MAX;

    for (auto worker : workers) {
        worker->setJob(word);
    }
    awaitAll();

    std::vector<result> results;

    for (auto worker : workers) {
        auto result = worker->getJobResult();
        if (result.distance == globalMin) {
            results.push_back(result);
        }
    }

    return results;
}

void Dictionary::awaitAll() {
    while (true) {
        bool loaded = true;
        for (auto worker : workers) {
            loaded = loaded && worker->isLoaded();
        }

        if (loaded) break;
    }
}


DictionaryWorker::DictionaryWorker() = default;

void DictionaryWorker::loadWords(unsigned char * buffer) {
    loadMutex.lock();
    buffers.push(buffer);
    loadMutex.unlock();
}


void DictionaryWorker::ThreadBody() {
        while (!IsThreadStopped()) {
        if (runningJob) {
            matchAgainstSingleLen(0);
            runningJob = false;
        }
        if (!buffers.empty()) {
            loadMutex.lock();
            if (buffers.empty()) {
                loadMutex.unlock();
                continue;
            }
            unsigned char * remoteBuffer = buffers.front();
            buffers.pop();
            loadMutex.unlock();
            auto string = new std::string();
            int i = 0;
            unsigned char c = remoteBuffer[i++];
            while (c != 0){

                if (caseInsensitive && c >= 'A' && c <= 'Z') {
                    c = c + ('a' - 'A');
                }

                if (c == '\n') {
                    words.push_back(string);
                    if ( lengthsMap.find(string->length()) == lengthsMap.end() ) {
                        auto vec = std::vector<std::string *>();
                        vec.push_back(string);
                        lengthsMap.insert({string->length(), vec});
                    } else {
                        lengthsMap.at(string->length()).push_back(string);
                    }
                    string = new std::string();
                } else {
                    string->push_back(c);
                }

                c = remoteBuffer[i++];
            }
            words.push_back(string);
            if ( lengthsMap.find(string->length()) == lengthsMap.end() ) {
                auto vec = std::vector<std::string *>();
                vec.push_back(string);
                lengthsMap.insert({string->length(), vec});
            } else {
                lengthsMap.at(string->length()).push_back(string);
            }
        }
    }
}

bool DictionaryWorker::isLoaded() {
    loadMutex.lock();
    const auto val = buffers.empty();
    loadMutex.unlock();
    return val && !runningJob;
}

unsigned long DictionaryWorker::getWordCount() {
    return words.size();
}

std::vector<std::string *> DictionaryWorker::getWords() {
    return words;
}

void DictionaryWorker::setJob(std::string word) {
    comparing.distance = INT32_MAX;
    comparing.value = std::move(word);
    runningJob = true;
}

result DictionaryWorker::getJobResult() {
    return comparing;
}

void DictionaryWorker::matchAgainstSingleLen(int len) {

    unsigned int localMin = globalMin;
    for (auto word : words) {
        if(std::abs((int)(word->length() - comparing.value.length())) > localMin) continue;
        const auto distance = Levenstein::calculate(*word, comparing.value, localMin, 0);

        if (distance < localMin || localMin > globalMin) {
            comparing.result[0] = nullptr;
            comparing.result[1] = nullptr;
            comparing.result[2] = nullptr;
            comparing.result[3] = nullptr;
            comparing.result[4] = nullptr;

            if (localMin > globalMin) {
                localMin = globalMin;
            }

            if (distance < localMin) {
                localMin = distance;
            }
        }

        if (distance == localMin) {
            comparing.distance = distance;
            if (comparing.result[0] == nullptr) comparing.result[0] = word;
            else if (comparing.result[1] == nullptr) comparing.result[1] = word;
            else if (comparing.result[2] == nullptr) comparing.result[2] = word;
            else if (comparing.result[3] == nullptr) comparing.result[3] = word;
            else if (comparing.result[4] == nullptr) comparing.result[4] = word;
        }

        if (globalMin > localMin) {
            globalMutex.lock();
            globalMin = localMin;
            globalMutex.unlock();
        }
    }
}
