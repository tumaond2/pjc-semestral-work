#include "../include/imrThread.h"
#include <exception>

using namespace imr;

Thread::Thread():
    mStopThread { true },
    mStopMutex  { }
{ }

Thread::~Thread()
{
    if( !IsThreadStopped() ){
        Stop();
        Join();
    }
}


void* Thread::ThreadBodyFunction(void* aThis)
{
    ((Thread *)aThis)->ThreadBody();
    return nullptr;
}

bool Thread::Start()
{
    // Set thread as joinable
    pthread_attr_t attr;
    pthread_attr_init( &attr );
    pthread_attr_setdetachstate( &attr, PTHREAD_CREATE_JOINABLE );
    // Create thread
    mStopThread = false;
    auto const status = pthread_create(&mThread, &attr, ThreadBodyFunction, this) == 0;
    // Free the attribute
    pthread_attr_destroy( &attr );

    return status;
}

void Thread::Stop()
{
    ScopeLock lock{mStopMutex};
    mStopThread = true;
}

void Thread::Join()
{
    pthread_join( mThread, nullptr );
}

bool Thread::IsThreadStopped() const
{
    bool result;
    {
        ScopeLock lock{mStopMutex};
        result = mStopThread;
    }
    return result;
}

Mutex::Mutex()
{
    auto rc = pthread_mutex_init( & mMutex, nullptr );
    if ( rc != 0 ) {
		throw std::runtime_error("Mutex::Mutex(): In constructor of Mutex pthread_mutex_init(&mMutex, NULL) failed. Error Code: " + std::to_string(rc));
    }
}

Mutex::~Mutex ()
{
    pthread_mutex_destroy(& mMutex);
}

void Mutex::Lock() const
{
    int rc = -1;
    rc = pthread_mutex_lock( & mMutex );
    if ( rc != 0 ){
		throw std::runtime_error( "Mutex::Lock(): pthread_mutex_lock failed. Error Code: " + std::to_string(rc) );
    }
}

void Mutex::TryLock() const
{
    auto rc = pthread_mutex_trylock( & mMutex );
    if ( rc != 0 ){
		throw std::runtime_error("pthread_mutex_trylock failed, already locked. Error Code: " + std::to_string(rc));
    }
}

void Mutex::Unlock() const
{
    auto rc = pthread_mutex_unlock ( & mMutex );
    if ( rc != 0 ){
		throw std::runtime_error("pthread_mutex_unlock failed. Error Code: " + std::to_string(rc));
    }
}

ScopeLock::ScopeLock(Mutex& aMutex) : mMutex(aMutex)
{
    mMutex.Lock();
}

ScopeLock::~ScopeLock()
{
    mMutex.Unlock();
}
