#include <iostream>
#include <Dictionary.h>
#include <CommandLineTools.h>
#include <Console.h>
#include <sstream>

#define DEFAULT_DICT "wordlists/wordlist_complete.txt"
#define DEFAULT_THREADS 2
#define DEFAULT_WORDS "anything i can parse like zymotechnical"


void printHelp() {
    std::cout << "HELP DOCS:" << '\n';
    std::cout << "-----------------------------------------------------" << '\n';
    std::cout << "Options:" << '\n';
    std::cout << "  SHORTCUT  |     DESCRIPTION            |        DEFAULT VALUE" << '\n';
    std::cout << "  -f              filename                        wordlist_complete.txt" << '\n';
    std::cout << "  -t              numberOfThreads                 2" << '\n';
    std::cout << "  --words         text to be corrected            testing" << '\n';
    std::cout << "  -c              case sensitive                  false" << '\n';
}

void getAndPrintCorrectedString(std::vector<std::string> words, Dictionary * dictionary) {
    for (auto word : words) {
        auto results = dictionary->getClosest(word);

        for (auto result : results) {
            if (result.distance == 0) {
                std::cout << Console::green(result.value);
                break;
            }
            for (auto res : result.result) {
                if (res == nullptr) continue;
                for (int i = 0; i < word.length(); i++) {
                    std::cout << (word[i] == (*res)[i] ? std::string(1, word[i]) : Console::red( res->length() - 1 >= i ? (*res)[i] : '-'));
                }
                break;
            }
            break;
        }
        std::cout << ' ';
    }
    std::cout << '\n';
}

int main(int argc, char *argv[]) {
    auto inputArgs = new InputParser(argc, argv);

    std::vector<std::string> knownOptions = {"-c", "-f", "-t", "--help", "--words"};
    auto unknownOptions = inputArgs->getUnknownOptions(knownOptions);
    if (!unknownOptions.empty()) {
        for (const auto& unknownOption : unknownOptions) {
            std::cout << "unknown option: " << unknownOption << '\n';
        }
        printHelp();
        exit(1);
    }

    bool caseSensitive = inputArgs->cmdOptionExists("-c");

    if (inputArgs->cmdOptionExists("--help")) {
        printHelp();
        exit(1);
    }

    std::string filename = inputArgs->getCmdOptionString("-f", DEFAULT_DICT);
    auto numThreads = inputArgs->getCmdOptionPositiveInt("-t", DEFAULT_THREADS);

    auto words = inputArgs->getCmdOptionStringAsArray("--words", DEFAULT_WORDS, caseSensitive);
    auto dictionary = new Dictionary(filename, numThreads);

    auto inputWords = inputArgs->getCmdOptionString("--words", DEFAULT_WORDS);
    std::cout << "input string:     " << inputWords << '\n';
    std::cout << "corrected string: ";
    getAndPrintCorrectedString(words, dictionary);

    return 0;
}
