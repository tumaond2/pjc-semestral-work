#ifndef IMR_THREAD_H
#define IMR_THREAD_H

#include <pthread.h>
#include <iostream>
#include <memory>

namespace imr {

/**
 * Wrapper class for pthread mutex class.
 */
class Mutex
{
public:
    /*
     * Constructor.
     */
    Mutex();

    /**
     * Destructor.
     */
    virtual ~Mutex();

    /**
     * Blocks until it locks.
     *
     * @throws std::runtime_error when lock fail
     */
    void Lock() const;

    /**
     * Tries to lock, throws when already locked.
     *
     * @throws std::runtime_error when already locked or lock failed
     */
    void TryLock() const;

    /**
     * Unlocks lock.
     *
     * @throws std::runtime_error when lock fail
     */
    void Unlock() const;


protected:
    /** pthread mutex structure */
    mutable pthread_mutex_t mMutex;
};

/**
 * RAII lock guard for Mutex.
 */
class ScopeLock
{
public:
    /**
     * Constructor.
     *
     * @param aMutex Used for locking
     */
    explicit ScopeLock(Mutex& aMutex);

    /**
     * Destructor.
     */
    ~ScopeLock();

    ScopeLock(ScopeLock const &) = delete;  ///< Cannot be copied
    void operator=(ScopeLock &) = delete;   ///< Cannot be assigned

private:
    Mutex & mMutex; ///< Lock reference
};


/**
 * Wrapper class for Thread.
 */
class Thread
{
public:
    /**
     * Constructor.
     */
    Thread();

    /**
     * Virtual destructor.
     */
    virtual ~Thread();

    /**
     * Creates new thread.
     * @return  [ @c True: thread was successfully started; @c False: otherwise ]
     */
    bool Start();

    /**
     * Signals thread to stop and prepare for join.
     */
    virtual void Stop();

    /**
     * Blocks until the thread is joined.
     */
    void Join();

    /**
     * Returns whether is the server thread marked to stop.
     * Can be used for breaking the cycles.
     */
    bool IsThreadStopped() const;

protected:
    /**
     * Virtual server thread body.
     * Implement this method in your subclass with the code you want your thread to run.
     */
    virtual void ThreadBody() = 0;

    bool mStopThread;            ///< whether thread has to end, should be manipulated only in Start() and Stop() functions
    mutable Mutex mStopMutex;    ///< Mutex for mStopThread

private:
    // Function that will pass server body pointer to create new thread
    static void* ThreadBodyFunction(void* aThis);


    // Thread object
    pthread_t mThread;
};

} // namespace imr

#endif // IMR_THREAD_H
