//
// Created by tumao on 11.01.22.
//

#ifndef AUTOCORRECT_CONSOLE_H
#define AUTOCORRECT_CONSOLE_H

#include <string>

class Console {
public:
    static std::string color (std::string word, int color, int part = 0);
    static std::string color (unsigned char word, int color, int part = 0);
    static std::string black (std::string word);
    static std::string red (std::string word);
    static std::string red (unsigned char word);
    static std::string green (std::string word);
    static std::string yellow (std::string word);
    static std::string gray (std::string word);
    static std::string lightGray (std::string word);
};

#endif //AUTOCORRECT_CONSOLE_H
