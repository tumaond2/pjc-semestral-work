//
// Created by tumao on 11.01.22.
//

#ifndef AUTOCORRECT_LEVENSTEIN_H
#define AUTOCORRECT_LEVENSTEIN_H

#include <string>

namespace Levenstein {
    static inline int calculate(std::string dict_word, std::string input_word, unsigned cut, unsigned size){
        if(dict_word.empty()) return input_word.size();
        if(input_word.empty()) return dict_word.size();
        if(dict_word[0] == input_word[0]) return Levenstein::calculate(dict_word.substr(1), input_word.substr(1), cut, size);
        if(cut <= ++size) return cut + 1;
        return 1 + std::min(
                        std::min(
                                    Levenstein::calculate(dict_word.substr(1), input_word, cut, size),
                                    Levenstein::calculate(dict_word, input_word.substr(1), cut, size)
                                ),
                        Levenstein::calculate(dict_word.substr(1),input_word.substr(1), cut, size)
                        );
    }
}

#endif //AUTOCORRECT_LEVENSTEIN_H
