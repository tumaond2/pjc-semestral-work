//
// Created by tumao on 02.01.22.
//

#ifndef FLEETCONTROL_COMMANDLINETOOLS_H
#define FLEETCONTROL_COMMANDLINETOOLS_H

#include <algorithm>
#include <iostream>
#include <string>
#include <vector>
#include <sstream>

bool isInteger(const std::string& s)
{
    std::string::const_iterator it = s.begin();
    while (it != s.end() && std::isdigit(*it)) ++it;
    return !s.empty() && it == s.end();
}

class InputParser{
public:
    InputParser (int &argc, char **argv){
        for (int i=1; i < argc; ++i)
            this->tokens.push_back(std::string(argv[i]));
    }

    const std::string& getCmdOptionString(const std::string &option, const std::string& defaultValue) const{
        for (int tokenIndex = 0; tokenIndex < tokens.size(); tokenIndex++) {

            auto token = tokens[tokenIndex];
            auto isMatched = token.find(option) != std::string::npos;

            if (isMatched) {
                if (tokenIndex != tokens.size() - 1) {
                    return tokens[tokenIndex + 1];
                } else {
                    std::cout << "option " << option << " has no option value specified" << '\n';
                    exit(1);
                }
            }
        }

        return defaultValue;
    }

    const std::vector<std::string> getCmdOptionStringAsArray(const std::string &option, const std::string& defaultValue, bool caseSensitive) const {
            auto inputWords = getCmdOptionString(option, defaultValue);
            char space_char = ' ';
            std::vector<std::string> words{};
            std::stringstream sstream(inputWords);
            std::string word;
            while (std::getline(sstream, word, space_char)){
                word.erase(std::remove_if(word.begin(), word.end(), ispunct), word.end());
                for (int i = 0; i < word.length(); i++) {
                    auto c = word[i];
                    if (!caseSensitive && c >= 'A' && c <= 'Z') {
                        c = c + ('a' - 'A');
                    }
                    word[i] = c;
                }

                words.push_back(word);
            }

            return words;
    }

    int getCmdOptionInt(const std::string &option, int defaultValue) const{
        std::string string;
        std::string varAsString = std::to_string(defaultValue);
        string = getCmdOptionString(option, varAsString);

        if (isInteger(string)) {
            return std::stoi(string);
        }

        std::cout << "option " << option << " value: \"" << string << "\" is not int-like" << '\n';
        exit(1);
    }

    int getCmdOptionPositiveInt(const std::string &option, int defaultValue) const{
        auto val = getCmdOptionInt(option, defaultValue);

        if (val <= 0) {
            std::cout << "option " << option << " value: \"" << val << "\" is not positive" << '\n';
            exit(1);
        }

        return val;
    }

    bool cmdOptionExists(const std::string &option) const{
        return std::find(this->tokens.begin(), this->tokens.end(), option)
               != this->tokens.end();
    }

    std::vector<std::string> getUnknownOptions(std::vector<std::string> options) {
        std::vector<std::string> unmatched;

        for (int tokenIndex = 0; tokenIndex < tokens.size(); tokenIndex++) {
            auto isMatched = false;
            auto token = tokens[tokenIndex];
            for (const auto& option : options) {

                isMatched = isMatched
                        || token.find(option) != std::string::npos;

                if (tokenIndex != 0) {
                    auto prevToken = tokens[tokenIndex - 1];
                    isMatched = isMatched || prevToken.find(option) != std::string::npos;
                }
            }
            if (!isMatched) {
                unmatched.push_back(token);
            }
        }

        return unmatched;
    }
private:
    std::vector <std::string> tokens;
};

#endif //FLEETCONTROL_COMMANDLINETOOLS_H
