//
// Created by tumao on 10.01.22.
//

#ifndef AUTOCORRECT_DICTIONARY_H
#define AUTOCORRECT_DICTIONARY_H

#define BUFFER_SIZE (1 << 10)
#define MIN_BLOCK_SIZE 256

#include <string>
#include <vector>
#include <queue>
#include <imrThread.h>
#include <fstream>
#include <mutex>
#include <Levenstein.h>
#include <map>

struct result {
    int distance = 0;
    std::string value = "";
    std::string * result[5] = {nullptr, nullptr, nullptr, nullptr, nullptr};
};



class DictionaryWorker: public imr::Thread {
private:
    std::vector<std::string *> words;
    std::queue<unsigned char *> buffers;
    std::mutex loadMutex;
    result comparing;
    bool runningJob = false;
    std::map<int, std::vector<std::string *>> lengthsMap;
    void matchAgainstSingleLen(int len);
public:
    explicit DictionaryWorker();
    void ThreadBody() override;
    void loadWords(unsigned char* buffer);
    std::vector<std::string *> getWords();
    bool isLoaded();
    unsigned long  getWordCount();
    void setJob(std::string word);
    result getJobResult();

};

class Dictionary {
private:
    std::vector<DictionaryWorker *> workers;
    void awaitAll();
    std::shared_ptr<int> compareMinimum;
public:
    Dictionary(std::string filename, unsigned int i);
    unsigned long getWordCount();

    std::vector<result> getClosest(const std::string& word);
};


#endif //AUTOCORRECT_DICTIONARY_H
